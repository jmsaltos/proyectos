"""Demo for use yolo v3
"""
import os
import time
import cv2
import numpy as np
from gtts import gTTS
from model.yolo_model import YOLO
from TextToSpeech import tts
from operator import itemgetter
from time import clock
from playsound import playsound

# from Clases.Clase24_10_2019.main import score, cl
from scipy.stats import stats


def process_image(img):
    """Resize, reduce and expand image.

    # Argument:
        img: original image.

    # Returns
        image: ndarray(64, 64, 3), processed image.
    """
    image = cv2.resize(img, (416, 416),
                       interpolation=cv2.INTER_CUBIC)
    image = np.array(image, dtype='float32')
    image /= 255.
    image = np.expand_dims(image, axis=0)

    return image


def get_classes(file):
    """Get classes name.

    # Argument:
        file: classes name for database.

    # Returns
        class_names: List, classes name.

    """
    with open(file) as f:
        class_names = f.readlines()
    class_names = [c.strip() for c in class_names]

    return class_names


def draw(image, boxes, scores, classes, all_classes):
    """Draw the boxes on the image.

    # Argument:
        image: original image.
        boxes: ndarray, boxes of objects.
        classes: ndarray, classes of objects.
        scores: ndarray, scores of objects.
        all_classes: all classes name.
    """
    lista1 = [];
    lista2 = [];
    for box, score, cl in zip(boxes, scores, classes):
        # contador = contador + 1
        x, y, w, h = box

        top = max(0, np.floor(x + 0.5).astype(int))
        left = max(0, np.floor(y + 0.5).astype(int))
        right = min(image.shape[1], np.floor(x + w + 0.5).astype(int))
        bottom = min(image.shape[0], np.floor(y + h + 0.5).astype(int))

        cv2.rectangle(image, (top, left), (right, bottom), (255, 0, 0), 2)
        cv2.putText(image, '{0} {1:.2f}'.format(all_classes[cl], score),
                    (top, left - 6),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    0.6, (0, 0, 255), 1,
                    cv2.LINE_AA)
        lista2.append(format(cl))
        temp = set(lista2)
        result = {}
        result2 = {}
        rss = {}
        s = ""
        for i in temp:
            result[i] = lista2.count(i)
        for k, v in result.items():
            if (v > 1):
                result2[(format(all_classes_plur[int(k)]))] = v
            else:
                lista1.append(format(all_classes[int(k)]))
                lista1 = list(format(lista1))
                listaux = list((lista1))
                h = (format(all_classes[int(k)]))
                if (listaux[-3] != "a"):
                    result2[(format(all_classes[int(k)]))] = "un"
                else:
                    result2[(format(all_classes[int(k)]))] = "una"
                lista1.clear()
        for k, v in result2.items():
            rss[str(v) + s] = k
            s += " "
        tts = gTTS(format(rss), lang='es-es')
    tts.save("SaveMp3/Leer.mp3")
    playsound('SaveMp3/Leer.mp3')
    os.remove('SaveMp3/Leer.mp3')


def detect_image(image, yolo, all_classes):
    """Use yolo v3 to detect images.

    # Argument:
        image: original image.
        yolo: YOLO, yolo model.
        all_classes: all classes name.

    # Returns:
        image: processed image.
    """
    pimage = process_image(image)

    start = time.time()
    boxes, classes, scores = yolo.predict(pimage, image.shape)
    end = time.time()

    print('time: {0:.2f}s'.format(end - start))
    if boxes is not None:
        draw(image, boxes, scores, classes, all_classes or all_classes_plur)

    return image


def detect_video(video, yolo, all_classes):
    """Use yolo v3 to detect video.

    # Argument:
        video: video file.
        yolo: YOLO, yolo model.
        all_classes: all classes name.
    """
    video_path = os.path.join("videos", "test", video)
    camera = cv2.VideoCapture(video_path)
    cv2.namedWindow("detection", cv2.WINDOW_AUTOSIZE)

    # Prepare for saving the detected video
    sz = (int(camera.get(cv2.CAP_PROP_FRAME_WIDTH)),
          int(camera.get(cv2.CAP_PROP_FRAME_HEIGHT)))
    fourcc = cv2.VideoWriter_fourcc(*'mpeg')

    vout = cv2.VideoWriter()
    vout.open(os.path.join("videos", "res", video), fourcc, 20, sz, True)

    while True:
        res, frame = camera.read()

        if not res:
            break

        image = detect_image(frame, yolo, all_classes)
        cv2.imshow("detection", image)

        # Save the video frame by frame
        vout.write(image)

        if cv2.waitKey(110) & 0xff == 27:
            break

    vout.release()
    camera.release()


if __name__ == '__main__':
    yolo = YOLO(0.6, 0.5)
    file = 'data/coco_classes_es.txt'
    file2 = 'data/coco_classes_es_plur.txt'
    all_classes = get_classes(file)
    all_classes_plur = get_classes(file2)
    a = 0
    TomFot = True
    while TomFot:
        b = clock()

        if b - a >2:
            print("Cada cinco segundos.")
            a = b

            cap = cv2.VideoCapture(0)

            leido, frame = cap.read()

            if leido == True:
                cv2.imwrite('./ImageCapture/foto.jpg', frame)
                print("Foto tomada correctamente")
            else:
                print("Error al acceder a la cámara")

            """
            	Finalmente liberamos o soltamos la cámara
            """
            # TomFot=False
            cap.release()

            for (root, dirs, files) in os.walk('./ImageCapture'):
                if files:
                    for f in files:
                        print(f)
                        path = os.path.join(root, f)
                        image = cv2.imread(path)
                        image = detect_image(image, yolo, all_classes or all_classes_plur)
                        cv2.imwrite('./OutImageCapture/' + f, image)
                        img1 = cv2.imread('OutImageCapture/foto.jpg')
                        cv2.imshow('IMAGEN', img1)
                        cv2.waitKey(1)
