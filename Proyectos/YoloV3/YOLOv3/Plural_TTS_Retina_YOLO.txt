---------------------RETINANET
.
.
.
.
.
.
R
E
T
I
N
A
N
E
T
.
.
.
.
..
.



#!/usr/bin/env python
# coding: utf-8

# ## Load necessary modules
# import keras
from operator import itemgetter

import keras

import sys

from future.types import newdict
from gtts import gTTS

sys.path.insert(0, '../')

# import keras_retinanet
from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from keras_retinanet.utils.visualization import draw_box, draw_caption
from keras_retinanet.utils.colors import label_color
from keras_retinanet.utils.gpu import setup_gpu
from TextToSpeech import tts

from time import clock
from playsound import playsound
# import miscellaneous modules
import matplotlib.pyplot as plt
import cv2
import os
import numpy as np
import time
from collections import Counter, defaultdict

# set tf backend to allow memory to grow, instead of claiming everything
import tensorflow as tf

# use this to change which GPU to use
gpu = 0

# set the modified tf session as backend in keras
setup_gpu(gpu)

# ## Load RetinaNet model

# In[ ]:


# adjust this to point to your downloaded/trained model
# models can be downloaded here: https://github.com/fizyr/keras-retinanet/releases
# model_path = os.path.join('..', 'snapshots', 'resnet50_coco_best_v2.1.0.h5')
model_path = os.path.join('..', 'snapshots', 'resnet50_coco_best_v2.1.0.h5')
# load retinanet model
model = models.load_model(model_path, backbone_name='resnet50')

# if the model is not converted to an inference model, use the line below
# see: https://github.com/fizyr/keras-retinanet#converting-a-training-model-to-inference-model
# model = models.convert_model(model)


# print(model.summary())

# load label to names mapping for visualization purposes
labels_to_names = {0: "persona", 1: 'bicicleta', 2: 'auto', 3: 'motocicleta', 4: 'avion', 5: 'autobus', 6: 'tren',
                   7: 'camion', 8: 'bote', 9: 'semaforo', 10: 'extintor', 11: 'señal de pare', 12: 'parquimetro',
                   13: 'banco', 14: 'pajaro', 15: 'gato', 16: 'perro', 17: 'caballo', 18: 'oveja', 19: 'vaca', 20: 'elefante',
                   21: 'oso', 22: 'cebra', 23: 'jirafa', 24: 'mochila', 25: 'paraguas', 26: 'bolso', 27: 'corbata',
                   28: 'maleta', 29: 'plato volador', 30: 'esquies', 31: 'patineta', 32: 'pelota deportiva', 33: 'cometa',
                   34: 'bate de beisbol', 35: 'guante de beisbol', 36: 'patineta', 37: 'tabla de surf', 38: 'raqueta de tenis',
                   39: 'botella', 40: 'copa de vino', 41: 'copa', 42: 'tenedor', 43: 'cuchillo', 44: 'cuchara', 45: 'tason',
                   46: 'banana', 47: 'manzana', 48: 'sanduche', 49: 'naranja', 50: 'brócoli', 51: 'zanahoria', 52: 'hot dog',
                   53: 'pizza', 54: 'rosquilla', 55: 'pastel', 56: 'silla', 57: 'sofá', 58: 'planta en maceta', 59: 'cama',
                   60: 'mesa de comedor', 61: 'inodoro', 62: 'televisor', 63: 'computadora portátil', 64: 'raton', 65: 'control remoto', 66: 'teclado',
                   67: 'teléfono celular', 68: 'microondas', 69: 'horno', 70: 'tostadora', 71: 'fregadero', 72: 'refrigerador',
                   73: 'libro', 74: 'reloj', 75: 'florero', 76: 'tijeras', 77: 'oso de peluche', 78: 'secador de pelo',
                   79: 'cepillo de dientes'}
labels_to_names_plural = {0: "personas", 1: 'bicicletas', 2: 'autos', 3: 'motocicletas', 4: 'aviones', 5: 'autobuses', 6: 'trenes',
                   7: 'camiones', 8: 'botes', 9: 'semaforos', 10: 'extintores', 11: 'señales de pare', 12: 'parquimetros',
                   13: 'bancos', 14: 'pajaros', 15: 'gatos', 16: 'perros', 17: 'caballos', 18: 'ovejas', 19: 'vacas', 20: 'elefantes',
                   21: 'osos', 22: 'cebras', 23: 'jirafas', 24: 'mochilas', 25: 'paraguas', 26: 'bolsos', 27: 'corbatas',
                   28: 'maletas', 29: 'platos voladores', 30: 'esquies', 31: 'patinetas', 32: 'pelotas deportivas', 33: 'cometas',
                   34: 'bates de beisbol', 35: 'guantes de beisbol', 36: 'patinetas', 37: 'tablas de surf', 38: 'raquetas de tenis',
                   39: 'botellas', 40: 'copas de vino', 41: 'copas', 42: 'tenedores', 43: 'cuchillos', 44: 'cucharas', 45: 'tasones',
                   46: 'bananas', 47: 'manzanas', 48: 'sánduches', 49: 'naranjas', 50: 'brócolis', 51: 'zanahorias', 52: 'perros calientes',
                   53: 'pizzas', 54: 'rosquillas', 55: 'pasteles', 56: 'sillas', 57: 'sofás', 58: 'plantas en maceta', 59: 'camas',
                   60: 'mesas de comedor', 61: 'inodoros', 62: 'televisores', 63: 'computadoras portátiles', 64: 'ratones', 65: 'controles remotos', 66: 'teclados',
                   67: 'teléfonos celulares', 68: 'microondass', 69: 'hornos', 70: 'tostadoras', 71: 'fregaderos', 72: 'refrigeradores',
                   73: 'libros', 74: 'relojes', 75: 'floreros', 76: 'tijeras', 77: 'osos de peluche', 78: 'secadores de pelo',
                   79: 'cepillos de dientes'}
# ## Run detection on example

# In[ ]:
a = 0
TomFot = True
total_object_count = 0
while TomFot:
    b = clock()
    image = read_image_bgr('ImageCapture/foto.jpg')
    if b - a >1:
        # print("Cada cinco segundos.")
        # print(type("{:}".format(labels_to_names[label])))
        a = b

        cap = cv2.VideoCapture(0)

        leido, image = cap.read()

        if leido == True:
            cv2.imwrite('./ImageCapture/foto.jpg', image)
            print("Foto tomada correctamente")
        else:
            print("Error al acceder a la cámara")

        """
            Finalmente liberamos o soltamos la cámara
        """
        # TomFot=False
        cap.release()

        for (root, dirs, files) in os.walk('./ImageCapture'):
            if files:
                for f in files:
                    print(f)
                    path = os.path.join(root, f)
                    image = cv2.imread(path)
                    # image = detect_image(image, yolo, all_classes)
                    # cv2.imwrite('images/res/' + f, image)


                    draw = image.copy()
                    # draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)


                    # preprocess image for network
                    image = preprocess_image(image)
                    image, scale = resize_image(image)

                    # process image
                    start = time.time()
                    boxes, scores, labels = model.predict_on_batch(np.expand_dims(image, axis=0))
                    print("processing time: ", time.time() - start)

                    # correct for image scale
                    boxes /= scale
                    zipDetections = zip(boxes[0], scores[0], labels[0])

                    lista2 = []
                    for box, score, label in zipDetections:
                        # scores are sorted so we can break

                        if score < 0.5:
                            break

                        color = label_color(label)

                        b = box.astype(int)
                        draw_box(draw, b, color=color)
                        caption = "{} {:.3f}".format(labels_to_names[label], score)
                        caption_name = str("{:}".format(labels_to_names[label]))
                        lista2.append(format(label))
                        temp = set(lista2)
                        result = {}
                        result2 = {}
                        rss = {}
                        s = ""
                        plt.figure(figsize=(15, 15))
                        plt.axis('off')
                        plt.imshow(draw)
                        cv2.imwrite('./OutImageCapture/foto.jpg', draw)
                        for i in (temp):
                            result[i] = lista2.count(i)
                        for k, v in result.items():
                            if (v > 1):
                                result2[(format(labels_to_names_plural[int(k)]))] = v
                            else:
                                result2[(format(labels_to_names[int(k)]))] = v
                        for k, v in result2.items():
                            rss[str(v)+s] = k
                            s+=" "
                        tts = gTTS(format(rss), lang='es')
                    tts.save("SaveMp3/Leer.mp3")
                    img1 = cv2.imread('OutImageCapture/foto.jpg')
                    cv2.imshow('IMAGEN', img1)
                    cv2.waitKey(1)
                    playsound('SaveMp3/Leer.mp3')
                    os.remove('SaveMp3/Leer.mp3')
                    draw_caption(draw, b, caption)









------------------------------------------------------------------------------YOLO--------------


*
*
*
*
**
*
Y
O
L
O

*
*
*




"""Demo for use yolo v3
"""
import os
import time
import cv2
import numpy as np
from gtts import gTTS
from model.yolo_model import YOLO
from TextToSpeech import tts
from operator import itemgetter
from time import clock
from playsound import playsound

# from Clases.Clase24_10_2019.main import score, cl
from scipy.stats import stats


def process_image(img):
    """Resize, reduce and expand image.

    # Argument:
        img: original image.

    # Returns
        image: ndarray(64, 64, 3), processed image.
    """
    image = cv2.resize(img, (416, 416),
                       interpolation=cv2.INTER_CUBIC)
    image = np.array(image, dtype='float32')
    image /= 255.
    image = np.expand_dims(image, axis=0)

    return image


def get_classes(file):
    """Get classes name.

    # Argument:
        file: classes name for database.

    # Returns
        class_names: List, classes name.

    """
    with open(file) as f:
        class_names = f.readlines()
    class_names = [c.strip() for c in class_names]

    return class_names


def draw(image, boxes, scores, classes, all_classes):
    """Draw the boxes on the image.

    # Argument:
        image: original image.
        boxes: ndarray, boxes of objects.
        classes: ndarray, classes of objects.
        scores: ndarray, scores of objects.
        all_classes: all classes name.
    """
    lista2 = [];
    for box, score, cl in zip(boxes, scores, classes):
        # contador = contador + 1
        x, y, w, h = box

        top = max(0, np.floor(x + 0.5).astype(int))
        left = max(0, np.floor(y + 0.5).astype(int))
        right = min(image.shape[1], np.floor(x + w + 0.5).astype(int))
        bottom = min(image.shape[0], np.floor(y + h + 0.5).astype(int))

        cv2.rectangle(image, (top, left), (right, bottom), (255, 0, 0), 2)
        cv2.putText(image, '{0} {1:.2f}'.format(all_classes[cl], score),
                    (top, left - 6),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    0.6, (0, 0, 255), 1,
                    cv2.LINE_AA)
        lista2.append(format(cl))
        temp = set(lista2)
        result = {}
        result2 = {}
        rss = {}
        s = ""
        for i in temp:
            result[i] = lista2.count(i)
        for k, v in result.items():
            if (v > 1):
                result2[(format(all_classes_plur[int(k)]))] = v
            else:
                result2[(format(all_classes[int(k)]))] = v
        for k, v in result2.items():
            rss[str(v) + s] = k
            s += " "
        tts = gTTS(format(rss), lang='es')
    tts.save("SaveMp3/Leer.mp3")
    playsound('SaveMp3/Leer.mp3')
    os.remove('SaveMp3/Leer.mp3')


def detect_image(image, yolo, all_classes):
    """Use yolo v3 to detect images.

    # Argument:
        image: original image.
        yolo: YOLO, yolo model.
        all_classes: all classes name.

    # Returns:
        image: processed image.
    """
    pimage = process_image(image)

    start = time.time()
    boxes, classes, scores = yolo.predict(pimage, image.shape)
    end = time.time()

    print('time: {0:.2f}s'.format(end - start))
    if boxes is not None:
        draw(image, boxes, scores, classes, all_classes or all_classes_plur)

    return image


def detect_video(video, yolo, all_classes):
    """Use yolo v3 to detect video.

    # Argument:
        video: video file.
        yolo: YOLO, yolo model.
        all_classes: all classes name.
    """
    video_path = os.path.join("videos", "test", video)
    camera = cv2.VideoCapture(video_path)
    cv2.namedWindow("detection", cv2.WINDOW_AUTOSIZE)

    # Prepare for saving the detected video
    sz = (int(camera.get(cv2.CAP_PROP_FRAME_WIDTH)),
          int(camera.get(cv2.CAP_PROP_FRAME_HEIGHT)))
    fourcc = cv2.VideoWriter_fourcc(*'mpeg')

    vout = cv2.VideoWriter()
    vout.open(os.path.join("videos", "res", video), fourcc, 20, sz, True)

    while True:
        res, frame = camera.read()

        if not res:
            break

        image = detect_image(frame, yolo, all_classes)
        cv2.imshow("detection", image)

        # Save the video frame by frame
        vout.write(image)

        if cv2.waitKey(110) & 0xff == 27:
            break

    vout.release()
    camera.release()


if __name__ == '__main__':
    yolo = YOLO(0.6, 0.5)
    file = 'data/coco_classes_es.txt'
    file2 = 'data/coco_classes_es_plur.txt'
    all_classes = get_classes(file)
    all_classes_plur = get_classes(file2)
    a = 0
    TomFot = True
    while TomFot:
        b = clock()

        if b - a >2:
            print("Cada cinco segundos.")
            a = b

            cap = cv2.VideoCapture(0)

            leido, frame = cap.read()

            if leido == True:
                cv2.imwrite('./ImageCapture/foto.jpg', frame)
                print("Foto tomada correctamente")
            else:
                print("Error al acceder a la cámara")

            """
            	Finalmente liberamos o soltamos la cámara
            """
            # TomFot=False
            cap.release()

            for (root, dirs, files) in os.walk('./ImageCapture'):
                if files:
                    for f in files:
                        print(f)
                        path = os.path.join(root, f)
                        image = cv2.imread(path)
                        image = detect_image(image, yolo, all_classes or all_classes_plur)
                        cv2.imwrite('./OutImageCapture/' + f, image)
                        img1 = cv2.imread('OutImageCapture/foto.jpg')
                        cv2.imshow('IMAGEN', img1)
                        cv2.waitKey(1)
