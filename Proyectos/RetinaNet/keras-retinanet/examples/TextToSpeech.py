from gtts import gTTS
from playsound import playsound

def tts(text_file, lang, name_file):
	with open(text_file, "r") as file:
		text = file.read()
	file = gTTS(text=text,lang=lang)
	filename = name_file
	file.save(filename)

# tts("SaveMp3/txtMp3.txt", "EN", "SaveMp3/txt1.mp3")
# playsound('SaveMp3/txt1.mp3')